.PHONY: proto dev build build-image

PWD = $(shell pwd)

VERSION = $(shell git describe --tags)
GITCOMMIT = $(shell git rev-parse --short HEAD)
GOVERSION=$(shell go version | cut -c 12-)
BUILD_LDFLAGS = "\
          -X \"gitlab.com/anondhateb/backend/version.GITCOMMIT=$(GITCOMMIT)\" \
          -X \"gitlab.com/anondhateb/backend/version.VERSION=$(VERSION)\" \
          -X \"gitlab.com/anondhateb/backend/version.GOVERSION=$(GOVERSION)\""
DOCKER_TAG ?= dev
DOCKER_REPO_URL = registry.gitlab.com/anondhateb/backend

proto:
	protoc -I protobuf/ protobuf/greeter.proto --go_out=plugins=grpc:./proto
	protoc -I protobuf/ protobuf/hateb.proto --go_out=plugins=grpc:./proto

dev:
	ANOND_HATEBU_BACKEND_DEBUG=DEBUG \
	go run -ldflags=$(BUILD_LDFLAGS) server.go

build:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
	go build -v -ldflags=$(BUILD_LDFLAGS) -o build/anondhateb-backend *.go

build_arm:
	GOOS=linux GOARCH=arm64 CGO_ENABLED=0 \
	go build -v -ldflags=$(BUILD_LDFLAGS) -o build/anondhateb-backend *.go

docker-login:
	docker login registry.gitlab.com

build-image:
	docker build --rm -t anondhateb-backend:$(DOCKER_TAG) .

push-image:
	docker tag anondhateb-backend:$(DOCKER_TAG) $(DOCKER_REPO_URL):$(DOCKER_TAG)
	docker push $(DOCKER_REPO_URL):$(DOCKER_TAG)