#
FROM alpine:3.13

RUN apk update
RUN apk add --no-cache ca-certificates

ADD build/anondhateb-backend /anondhateb-backend

# Add grpc-health-probe to use with readiness and liveness probes
RUN GRPC_HEALTH_PROBE_VERSION=v0.3.2 && \
    GOARCH=arm64 && \
    wget -q -O /bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-${GOARCH} && \
    chmod +x /bin/grpc_health_probe

# Is `/bin/anondhateb-backend` better ?
ENTRYPOINT ["/anondhateb-backend"]
