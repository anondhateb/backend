package main

import (
	"log"
	"net"

	"gitlab.com/anondhateb/backend/hateb"
	"gitlab.com/anondhateb/backend/health"
	"gitlab.com/anondhateb/backend/hello"
	pb "gitlab.com/anondhateb/backend/proto"

	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
)

func main() {
	addr := ":8088"
	lis, err := net.Listen("tcp", addr)

	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterGreeterServer(s, hello.NewServer())
	pb.RegisterHatebServer(s, hateb.NewServer())
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())

	log.Printf("gRPC server listening on : %v", addr)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
