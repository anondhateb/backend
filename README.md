# backend

## Prerequisite tool
- go 1.14
- terraform 0.12
  - service account on google cloud

## add protobuf module as submodule
git submodule add https://gitlab.com/anondhateb/protobuf protobuf

## Project Image

![backend image](https://cacoo.com/diagrams/RBtUb8vYWH6hoYXk-5CE93.png)

## Provisioning
Check following directory
- terraform
- k8s