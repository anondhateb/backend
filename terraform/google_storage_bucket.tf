# https://cloud.google.com/storage/docs/hosting-static-website
resource "google_storage_bucket" "frontend" {
  name          = "static.anondhateb.com"
  location      = "ASIA"
  force_destroy = true

  bucket_policy_only = true

  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
}

resource "google_storage_bucket_iam_binding" "frontend" {
  bucket = google_storage_bucket.frontend.name
  role = "roles/storage.legacyObjectReader"
  members = [
    "allUsers",
  ]
}

resource "google_compute_backend_bucket" "frontend" {
  name        = "cdn-frontend-bucket"
  bucket_name = google_storage_bucket.frontend.name
  enable_cdn  = true
}