provider "google" {
  project = "anondhateb"
  region  = "asia-northeast1"
  zone    = "asia-northeast1-c"
}

provider "google-beta" {
  project = "anondhateb"
  region  = "asia-northeast1"
  zone    = "asia-northeast1-c"
}