### main

resource "google_dns_managed_zone" "main-zone" {
  name     = "main-zone"
  dns_name = "anondhateb.com."
  visibility = "public"
}

resource "google_dns_record_set" "api-ns" {
  name         = "api.${google_dns_managed_zone.main-zone.dns_name}"
  managed_zone = google_dns_managed_zone.main-zone.name
  type         = "NS"
  ttl          = 300

  rrdatas = [
    "ns-cloud-a1.googledomains.com.",
    "ns-cloud-a2.googledomains.com.",
    "ns-cloud-a3.googledomains.com.",
    "ns-cloud-a4.googledomains.com.",
  ]
}

resource "google_dns_record_set" "static" {
  name         = "static.${google_dns_managed_zone.main-zone.dns_name}"
  managed_zone = google_dns_managed_zone.main-zone.name
  type         = "A"
  ttl          = 300

  rrdatas      = [google_compute_global_forwarding_rule.cdn_rule.ip_address]
}

### api

resource "google_dns_managed_zone" "api-zone" {
  name     = "api-zone"
  dns_name = "api.anondhateb.com."
  visibility = "public"
}

resource "google_dns_record_set" "api" {
  name         = google_dns_managed_zone.api-zone.dns_name
  managed_zone = google_dns_managed_zone.api-zone.name
  type         = "A"
  ttl          = 300

  rrdatas = ["35.187.219.73"]
}

resource "google_dns_record_set" "api-acme-challenge" {
  name         = "_acme-challenge.${google_dns_managed_zone.api-zone.dns_name}"
  managed_zone = google_dns_managed_zone.api-zone.name
  type         = "TXT"
  ttl          = 300

  rrdatas = ["BHgNoZ62mlrRQULz4FYWh8I6Sj2R5NEBGAeRiPkjy8Y"]
}