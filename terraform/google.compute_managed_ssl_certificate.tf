resource "google_compute_managed_ssl_certificate" "cdn_certificate" {
  provider = google-beta
  name = "cdn-managed-certificate"
 
  managed {
    domains = ["static.anondhateb.com."]
  }
}

resource "google_compute_target_https_proxy" "cdn_https_proxy" {
  name             = "cdn-https-proxy"
  url_map          = google_compute_url_map.cdn_url_map.self_link
  ssl_certificates = [google_compute_managed_ssl_certificate.cdn_certificate.self_link]
}

resource "google_compute_url_map" "cdn_url_map" {
  name            = "cdn-url-map"
  default_service = google_compute_backend_bucket.frontend.self_link

  host_rule {
    hosts        = ["static.anondhateb.com"]
    path_matcher = "allpaths"
  }

  path_matcher {
    name            = "allpaths"
    default_service = google_compute_backend_bucket.frontend.self_link

    path_rule {
      paths   = ["/*"]
      service = google_compute_backend_bucket.frontend.self_link
    }
  }
}

resource "google_compute_global_forwarding_rule" "cdn_rule" {
  provider = google-beta

  name       = "forwarding-rule"
  target     = google_compute_target_https_proxy.cdn_https_proxy.self_link
  port_range = 443
}