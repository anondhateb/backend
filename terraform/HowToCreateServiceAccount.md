## This doc show how to create service account

## What is service account ?
Service account is an account when you use terraform. You need to create this one once.

## Enable services
```
$ gcloud services enable compute.googleapis.com 
```

```
$ gcloud services enable cloudresourcemanager.googleapis.com
```

## Create a service account for terraform
```
$ gcloud iam service-accounts create terraform-service-account \
    --project=anondhateb \
    --display-name terraform-service-account
```

```
$ gcloud iam service-accounts list \
    --project=anondhateb \
    --filter=displayName:terraform-service-account \
    --format='value(email)'
terraform-service-account@anondhateb.iam.gserviceaccount.com
```

Note:
Just create a service account especially for terraform.
Use terraform for others service account.

## Binding policy
You need to bind policy you want.

```
// iam.serviceAccountUser
$ gcloud projects add-iam-policy-binding anondhateb \
    --role roles/iam.serviceAccountUser \
    --member serviceAccount:terraform-service-account@anondhateb.iam.gserviceaccount.com

// iam.serviceAccountCreator
$ gcloud projects add-iam-policy-binding anondhateb \
    --role roles/iam.serviceAccountCreator \
    --member serviceAccount:terraform-service-account@anondhateb.iam.gserviceaccount.com

// iam.serviceAccountAdmin
$ gcloud projects add-iam-policy-binding anondhateb \
    --role roles/iam.serviceAccountAdmin \
    --member serviceAccount:terraform-service-account@anondhateb.iam.gserviceaccount.com

// iam.roleAdmin
$ gcloud projects add-iam-policy-binding anondhateb \
    --role roles/iam.roleAdmin \
    --member serviceAccount:terraform-service-account@anondhateb.iam.gserviceaccount.com

// resourcemanager.projectIamAdmin
$ gcloud projects add-iam-policy-binding anondhateb \
    --role roles/resourcemanager.projectIamAdmin \
    --member serviceAccount:terraform-service-account@anondhateb.iam.gserviceaccount.com

// compute.admin
$ gcloud projects add-iam-policy-binding anondhateb \
    --role roles/compute.admin \
    --member serviceAccount:terraform-service-account@anondhateb.iam.gserviceaccount.com

// storage.admin
$ gcloud projects add-iam-policy-binding anondhateb \
    --role roles/storage.admin \
    --member serviceAccount:terraform-service-account@anondhateb.iam.gserviceaccount.com

// container.clusterAdmin
$ gcloud projects add-iam-policy-binding anondhateb \
    --role roles/container.clusterAdmin \
    --member serviceAccount:terraform-service-account@anondhateb.iam.gserviceaccount.com

// dns.admin
$ gcloud projects add-iam-policy-binding anondhateb \
    --role roles/dns.admin \
    --member serviceAccount:terraform-service-account@anondhateb.iam.gserviceaccount.com
```

```
// remove-iam-policy-binding
$ gcloud projects remove-iam-policy-binding anondhateb \
--member serviceAccount:terraform-service-account@anondhateb.iam.gserviceaccount.com \
--role roles/iam.roleViewer

// check your policy
$ gcloud projects get-iam-policy anondhateb
```

## Create token for service account
```
$ gcloud iam service-accounts keys create ~/.gcp/terraform-service-account.json \
    --iam-account terraform-service-account@anondhateb.iam.gserviceaccount.com
```

```
$ gcloud iam service-accounts keys create ~/.gcp/gitlab-service-account.json \
    --iam-account gitlab-service-account@anondhateb.iam.gserviceaccount.com
```