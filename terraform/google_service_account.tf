resource "google_service_account" "gitlab_service_account" {
  account_id   = "gitlab-service-account"
  display_name = "gitlab service account"
}

resource "google_project_iam_member" "gitlab-account-iam" {
  role = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.gitlab_service_account.email}"
}