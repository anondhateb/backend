## Prerequisite tool
- grpcurl

## Hello service

### Hello in local
grpcurl -v -d '{"name": "bar"}' -plaintext -proto protobuf/greeter.proto localhost:8088 proto.Greeter/SayHello


### Hello with k8s
EXTERNAL_IP=$(kubectl get service envoy -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
grpcurl -d '{"name": "bbb"}' -proto protobuf/greeter.proto -insecure -v $EXTERNAL_IP:443 proto.Greeter/SayHello


### Hello with URL
grpcurl -d '{"name": "bbb"}' -proto protobuf/greeter.proto -insecure -v api.anondhateb.com:443 proto.Greeter/SayHello

grpcurl -d '{"name": "bbb"}' -proto protobuf/greeter.proto -v api.anondhateb.com:443 proto.Greeter/SayHello

## Hateb Feed service

### Feed in local
grpcurl -v -plaintext -proto protobuf/hateb.proto localhost:8088 proto.Hateb/GetFeed


### Feed with URL
grpcurl -proto protobuf/hateb.proto -insecure -v api.anondhateb.com:443 proto.Hateb/GetFeed

grpcurl -proto protobuf/hateb.proto -v api.anondhateb.com:443 proto.Hateb/GetFeed

## Hateb Comment service

### Hateb Comment in local
grpcurl -d '{"url": "https://b.toenobu.name/ja/posts/get-university-of-london/"}' -v -plaintext -proto protobuf/hateb.proto localhost:8088 proto.Hateb/GetBookMarkComment


### Hateb Comment with URL
grpcurl -d '{"url": "https://b.toenobu.name/ja/posts/get-university-of-london/"}' -proto protobuf/hateb.proto -insecure -v api.anondhateb.com:443 proto.Hateb/GetBookMarkComment

grpcurl -d '{"url": "https://b.toenobu.name/ja/posts/get-university-of-london/"}' -proto protobuf/hateb.proto -v api.anondhateb.com:443 proto.Hateb/GetBookMarkComment