package comment

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type Bookmark struct {
	Title   string    `json:"title"`
	Comment []Comment `json:"bookmarks"`
}

type Comment struct {
	User    string `json:"user"`
	Comment string `json:"comment"`
}

var HATEB_URL string = "https://b.hatena.ne.jp/entry/json/{URL}"

func (b *Bookmark) NewRequest(url string) *http.Request {
	// New Request
	replace_url := strings.Replace(HATEB_URL, "{URL}", url, 1)
	req, err := http.NewRequest("GET", replace_url, nil)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("User-Agent", "anondhateb/1.0 web-application")

	return req
}

func (b *Bookmark) Send(req *http.Request) error {
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	json.Unmarshal(body, b)
	return nil
}
