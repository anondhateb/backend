package bookmark

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
)

type Bookmark struct {
	Title string `xml:"title"`
	Date  string `xml:"date"`
	Link  string `xml:"link"`
}

type Feed struct {
	Bookmark []Bookmark `xml:"item"`
}

var HATEB_URL string = "https://b.hatena.ne.jp/hotentry/all.rss"

func (f *Feed) NewRequest() *http.Request {
	// New Request
	req, err := http.NewRequest("GET", HATEB_URL, nil)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("User-Agent", "anondhateb/1.0 web-application")

	return req
}

func (f *Feed) Send(req *http.Request) error {
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	xml.Unmarshal(body, f)
	return nil
}
