package hateb

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/anondhateb/backend/hateb/bookmark"
	"gitlab.com/anondhateb/backend/hateb/comment"
	pb "gitlab.com/anondhateb/backend/proto"
)

type Server struct{}

func NewServer() *Server {
	s := &Server{}
	return s
}

func (s *Server) GetFeed(ctx context.Context, in *pb.FeedRequest) (*pb.FeedReply, error) {
	log.Printf("Received: request!!!")

	feed, err := GetFeedByHTTP()
	if err != nil {
		log.Fatalln(err)
	}

	feedReply := &pb.FeedReply{Item: []*pb.BookMarkReply{}}
	for _, bm := range feed.Bookmark {
		pbbm := &pb.BookMarkReply{}

		pbbm.Title = bm.Title

		t, err := time.Parse(time.RFC3339, bm.Date)
		if err != nil {
			log.Println(err)
		}

		pbbm.Date = &timestamp.Timestamp{Seconds: t.Unix()}
		pbbm.Link = bm.Link

		feedReply.Item = append(feedReply.Item, pbbm)
	}

	return feedReply, nil
}

func GetFeedByHTTP() (*bookmark.Feed, error) {
	f := &bookmark.Feed{}

	req := f.NewRequest()
	f.Send(req)

	return f, nil
}

func (s *Server) GetBookMarkComment(ctx context.Context, in *pb.BookMarkCommentRequest) (*pb.BookMarkCommentReply, error) {
	log.Printf("Received: comment request!!!")

	bookmark, err := GetCommentByHTTP(in.GetUrl())
	if err != nil {
		log.Fatalln(err)
	}

	bookmarkCommentReply := &pb.BookMarkCommentReply{Title: bookmark.Title, Comment: []*pb.CommentReply{}}
	for _, co := range bookmark.Comment {
		pbco := &pb.CommentReply{}
		fmt.Println(co)

		pbco.User = co.User

		if co.Comment == "" {
			continue
		}

		pbco.Commnet = co.Comment

		bookmarkCommentReply.Comment = append(bookmarkCommentReply.Comment, pbco)
	}

	return bookmarkCommentReply, nil
}

func GetCommentByHTTP(url string) (*comment.Bookmark, error) {
	b := &comment.Bookmark{}

	req := b.NewRequest(url)
	b.Send(req)

	return b, nil
}

func Println() {
	feed, _ := GetFeedByHTTP()
	for _, b := range feed.Bookmark {
		fmt.Println(b.Title)
	}
}
