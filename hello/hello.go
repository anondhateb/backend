package hello

import (
	"context"
	"log"

	pb "gitlab.com/anondhateb/backend/proto"
)

type Server struct{}

func NewServer() *Server {
	s := &Server{}
	return s
}

func (s *Server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	log.Printf("Received: %v", in.Name)
	return &pb.HelloReply{Message: "Hello " + in.Name}, nil
}
